const express = require('express');
const app = express();
const path = require('path');
const request = require('request');
const rp = require('request-promise');
var fs = require('fs');
var getJSON = require('get-json');
const $ = require('jquery');
var sleep = require('system-sleep');
const https = require('https');

//Require modules, for reference in the custom function
const compareRequest = require(__dirname + '/functions/addProducts');


//Start Server
app.listen(3000, () => console.log('App listening on port 3000!'));


//Keep This if uploading to a server
/*
https.createServer({
    key: fs.readFileSync('key.pem'),
    cert: fs.readFileSync('cert.pem')
}, app).listen(3000);

app.get('/', function (req, res) {
    res.header('Content-type', 'text/html');
    return res.end('<h1>Hello, Secure World!</h1>');
});
*/

//Call Custom Request
customRequest();

function customRequest(){
    //Compare and Replace
    compareRequest.compareAndAdd(); 
}










