const express = require('express');
const app = express();
const path = require('path');
const request = require('request');
const rp = require('request-promise');
var fs = require('fs');
var getJSON = require('get-json');
const $ = require('jquery');
var sleep = require('system-sleep');
var cron = require('node-cron');

var imgArray = {};
var arrayOfProducts = {
    devices: {
        iX: {
            label : "iPhone X",
            type : "iX",
            imageDetails: {
                mark64 : "aHR0cHM6Ly9zMy5hbWF6b25hd3MuY29tL2NhcnZlZC1vdmVybGF5cy9vdmVybGF5X2lwaG9uZXgucG5n",
                markx : 0,
                marky : -20,
                rect : "1,1,600,900",
                w : 500,
                h : 700,
                fit : "fill",
                bg : "ffffff",
                markfit : "crop",
                markw : 500,
                markh : 750
            }
        },
        i6_7_8: {
            label: "iPhone 8 / 7 / 6",
            type: "i6_7_8",
            imageDetails: {
                mark64 : "aHR0cHM6Ly9zMy5hbWF6b25hd3MuY29tL2NhcnZlZC1vdmVybGF5cy9vdmVybGF5X2lwaG9uZTg3Ni5wbmc",
                markx : 0,
                marky : -10,
                rect : "10,15,500,995",
                w : 500,
                h : 700,
                fit : "fill",
                bg : "ffffff",
                markfit : "crop",
                markw : 500,
                markh : 750
            }
        },
        i6_7_8plus: {
            label: "iPhone 8 / 7 / 6 Plus",
            type: "i6_7_8plus",
            imageDetails: {
                mark64 : "aHR0cHM6Ly9zMy5hbWF6b25hd3MuY29tL2NhcnZlZC1vdmVybGF5cy9vdmVybGF5X2lwaG9uZTg3Ni5wbmc",
                markx : 0,
                marky : -10,
                rect : "10,15,500,995",
                w : 500,
                h : 700,
                fit : "fill",
                bg : "ffffff",
                markfit : "crop",
                markw : 500,
                markh : 750
            }
        },
        iSE: {
            label: "iPhone SE",
            type: "iSE",
            imageDetails: {
                mark64 : "aHR0cHM6Ly9zMy5hbWF6b25hd3MuY29tL2NhcnZlZC1vdmVybGF5cy9vdmVybGF5X2lwaG9uZXNlLnBuZw",
                markx : 0,
                marky : -10,
                rect : "10,15,500,995",
                w : 500,
                h : 700,
                fit : "fill",
                bg : "ffffff",
                markfit : "crop",
                markw : 500,
                markh : 750
            }
        },
        galaxyS9: {
            label: "Samsung Galaxy S9",
            type: "galaxyS9",
            imageDetails: {
                mark64 : "aHR0cHM6Ly9zMy5hbWF6b25hd3MuY29tL2NhcnZlZC1vdmVybGF5cy9vdmVybGF5X2dhbGF4eXM5LnBuZw",
                markx : 0,
                marky : -10,
                rect : "10,15,500,995",
                w : 500,
                h : 700,
                fit : "fill",
                bg : "ffffff",
                markfit : "crop",
                markw : 500,
                markh : 750
            }
        },
        galaxyS9Plus: {
            label: "Samsung Galaxy S9 Plus",
            type: "galaxyS9Plus",
            imageDetails: {
                mark64 : "aHR0cHM6Ly9zMy5hbWF6b25hd3MuY29tL2NhcnZlZC1vdmVybGF5cy9vdmVybGF5X2dhbGF4eXM5cGx1cy5wbmc",
                markx : 0,
                marky : -10,
                rect : "10,15,500,995",
                w : 500,
                h : 700,
                fit : "fill",
                bg : "ffffff",
                markfit : "crop",
                markw : 500,
                markh : 750
            }
        },
        galaxyNote8: {
            label: "Samsung Galaxy Note 8",
            type: "galaxyNote8",
            imageDetails: {
                mark64 : "aHR0cHM6Ly9zMy5hbWF6b25hd3MuY29tL2NhcnZlZC1vdmVybGF5cy9vdmVybGF5X2dhbGF4eW5vdGU4LnBuZw",
                markx : 0,
                marky : -10,
                rect : "10,15,500,995",
                w : 500,
                h : 700,
                fit : "fill",
                bg : "ffffff",
                markfit : "crop",
                markw : 500,
                markh : 750
            }
        },
        galaxyS8Plus: {
            label: "Samsung Galaxy S8 Plus",
            type: "galaxyS8Plus",
            imageDetails: {
                mark64 : "aHR0cHM6Ly9zMy5hbWF6b25hd3MuY29tL2NhcnZlZC1vdmVybGF5cy9vdmVybGF5X2dhbGF4eXM4cGx1cy5wbmc",
                markx : 0,
                marky : -10,
                rect : "10,15,500,995",
                w : 500,
                h : 700,
                fit : "fill",
                bg : "ffffff",
                markfit : "crop",
                markw : 500,
                markh : 750
            }
        },
        galaxyS8: {
            label: "Samsung Galaxy S8",
            type: "galaxyS8",
            imageDetails: {
                mark64 : "aHR0cHM6Ly9zMy5hbWF6b25hd3MuY29tL2NhcnZlZC1vdmVybGF5cy9vdmVybGF5X2dhbGF4eXM4LnBuZw",
                markx : 0,
                marky : -10,
                rect : "10,15,500,995",
                w : 500,
                h : 700,
                fit : "fill",
                bg : "ffffff",
                markfit : "crop",
                markw : 500,
                markh : 750
            }
        },
        galaxyS7Edge: {
            label: "Samsung Galaxy S7 Edge",
            type: "galaxyS7Edge",
            imageDetails: {
                mark64 : "aHR0cHM6Ly9zMy5hbWF6b25hd3MuY29tL2NhcnZlZC1vdmVybGF5cy9vdmVybGF5X2dhbGF4eXM3ZWRnZS5wbmc",
                markx : 0,
                marky : -10,
                rect : "10,15,500,995",
                w : 500,
                h : 700,
                fit : "fill",
                bg : "ffffff",
                markfit : "crop",
                markw : 500,
                markh : 750
            }
        },
        galaxyS7: {
            label: "Samsung Galaxy S7",
            type: "galaxyS7",
            imageDetails: {
                mark64 : "aHR0cHM6Ly9zMy5hbWF6b25hd3MuY29tL2NhcnZlZC1vdmVybGF5cy9vdmVybGF5X2dhbGF4eXM3LnBuZw",
                markx : 0,
                marky : -10,
                rect : "10,15,500,995",
                w : 500,
                h : 700,
                fit : "fill",
                bg : "ffffff",
                markfit : "crop",
                markw : 500,
                markh : 750
            }
        },
        galaxyS6Edge: {
            label: "Samsung Galaxy S6Edge",
            type: "galaxyS6Edge",
            imageDetails: {
                mark64 : "aHR0cHM6Ly9zMy5hbWF6b25hd3MuY29tL2NhcnZlZC1vdmVybGF5cy9vdmVybGF5X2dhbGF4eXM2ZWRnZS5wbmc",
                markx : 0,
                marky : -10,
                rect : "10,15,500,995",
                w : 500,
                h : 700,
                fit : "fill",
                bg : "ffffff",
                markfit : "crop",
                markw : 500,
                markh : 750
            }
        },
        galaxyS6: {
            label: "Samsung Galaxy S6",
            type: "galaxyS6",
            imageDetails: {
                mark64 : "aHR0cHM6Ly9zMy5hbWF6b25hd3MuY29tL2NhcnZlZC1vdmVybGF5cy9vdmVybGF5X2dhbGF4eXM2LnBuZw",
                markx : 0,
                marky : -10,
                rect : "10,15,500,995",
                w : 500,
                h : 700,
                fit : "fill",
                bg : "ffffff",
                markfit : "crop",
                markw : 500,
                markh : 750
            }
        },
        googlePixel: {
            label: "Google Pixel",
            type: "googlePixel",
            imageDetails: {
                mark64 : "aHR0cHM6Ly9zMy5hbWF6b25hd3MuY29tL2NhcnZlZC1vdmVybGF5cy9vdmVybGF5X3BpeGVsLnBuZw",
                markx : 0,
                marky : -10,
                rect : "10,15,500,995",
                w : 500,
                h : 700,
                fit : "fill",
                bg : "ffffff",
                markfit : "crop",
                markw : 500,
                markh : 750
            }
        },
        googlePixel2: {
            label: "Google Pixel 2",
            type: "googlePixel2",
            imageDetails: {
                mark64 : "aHR0cHM6Ly9zMy5hbWF6b25hd3MuY29tL2NhcnZlZC1vdmVybGF5cy9vdmVybGF5X3BpeGVsMi5wbmc",
                markx : 0,
                marky : -10,
                rect : "10,15,500,995",
                w : 500,
                h : 700,
                fit : "fill",
                bg : "ffffff",
                markfit : "crop",
                markw : 500,
                markh : 750
            }
        },
        googlePixel2XL: {
            label: "Google Pixel 2 XL",
            type: "googlePixel2XL",
            imageDetails: {
                mark64 : "aHR0cHM6Ly9zMy5hbWF6b25hd3MuY29tL2NhcnZlZC1vdmVybGF5cy9vdmVybGF5X3BpeGVsMnhsLnBuZw",
                markx : 0,
                marky : -10,
                rect : "10,15,500,995",
                w : 500,
                h : 700,
                fit : "fill",
                bg : "ffffff",
                markfit : "crop",
                markw : 500,
                markh : 750
            }
        },
        googlePixelXL: {
            label: "Google Pixel XL",
            type: "googlePixelXL",
            imageDetails: {
                mark64 : "aHR0cHM6Ly9zMy5hbWF6b25hd3MuY29tL2NhcnZlZC1vdmVybGF5cy9vdmVybGF5X3BpeGVseGwucG5n",
                markx : 0,
                marky : -10,
                rect : "10,15,500,995",
                w : 500,
                h : 700,
                fit : "fill",
                bg : "ffffff",
                markfit : "crop",
                markw : 500,
                markh : 750
            }
        },
        powerbank: {
            label: "Powerbank",
            type: "powerbank",
            imageDetails: {
                mark64 : "aHR0cHM6Ly9zMy5hbWF6b25hd3MuY29tL2NhcnZlZC1vdmVybGF5cy9vdmVybGF5X3Bvd2VyYmFuay5wbmc",
                markx : 0,
                marky : -10,
                rect : "10,15,500,995",
                w : 500,
                h : 700,
                fit : "fill",
                bg : "ffffff",
                markfit : "crop",
                markw : 500,
                markh : 750
            }
        },
    }
}

var task = cron.schedule('*/1 * * * *', function() {
    //grabFromIO(ioArray,variant,variantInner);
    //correctIds();
});
task.stop();

var ioArray = [];
var variant = [];
var variantInner = [];
var variantArray = [];

grabFromIO(imgArray,ioArray,variant,variantInner,arrayOfProducts);

module.exports={
    compareAndAdd: function(){
        task.start();
    }
}

//Grab from carved IO
function grabFromIO(imgArray,ioArray,variant,variantInner,arrayOfProducts,variantArray){
    request('https://satellite.carved.io/available', { json: true }, function(err, res, body){
        var x = 0;
        for(var i = 0; i < 1/*body.length*/; i++){
            variantInner = {};
            variant = [];
            variantArray = [];
            var id = body[i].gen_id;
            var name = body[i].name;
            var collectionID = body[i].collection_id._id;
            var includeID = "5824758a10bc20b644961688";
            var countingNumber = 0;
            var metafieldsArray = [];
            if(collectionID !== includeID){
            } 
            else{
                for (var x in arrayOfProducts.devices) {
                    variantInner = {};
                    var metafields = {};
                    var imgsrc = body[i].image;
                    var lengthOfObj = Object.keys(arrayOfProducts.devices).length;
                    var details = (arrayOfProducts.devices[x]);
                    var imageDetail = details.imageDetails;
                    var rect = imageDetail.rect;
                    var mark64 = imageDetail.mark64;
                    var markx = imageDetail.markx;
                    var marky = imageDetail.marky;
                    var markfit = imageDetail.markfit;
                    var markw = imageDetail.markw;
                    var markh = imageDetail.markh
                    var w = imageDetail.w;
                    var h = imageDetail.h;
                    var fit = imageDetail.fit;
                    var bg = imageDetail.bg;
                    var title = details.label;
                    var type = details.type;
                    var sku = variant.sku;
                    var imgsrc = imgsrc.replace("https://s3.amazonaws.com/carved-wildwoods-distillery/","https://carved.imgix.net/");
                    imgsrc = imgsrc + "?rect=" + rect + "&w=" + w + "&h=" + h + "&fit=" + fit + "&bg=" + bg + "&markfit=" + markfit + "&markx=" + markx + "&marky=" + marky + "&markw=" + markw + "&markh=" + markh + "&mark64=" + mark64;
                    var sku = name + "-" + id + "-" + type;
                    var price = body[i].price;
                    nameID = name +  " - " + id;
                    variantInner["option1"] = title;
                    metafields = 
                        {
                        key: "link",
                        value: imgsrc,
                        value_type: "string",
                        namespace: "Type: " + type
                    }

                    variantInner["sku"] = sku;
                    variantInner["inventory_quantity"] = 1;
                    variantInner["inventory_management"] = "shopify";
                    variantInner["inventory_policy"] = "deny";
                    variantInner["price"] = price;
                    variantArray.push(variantInner);
                    metafieldsArray.push(metafields);
                    if(countingNumber == lengthOfObj - 1){
                        assembleArray(imgArray,variantArray,arrayOfProducts,imgsrc,nameID,metafieldsArray,id);
                    }
                    countingNumber++;
                }
            }
        }
    })
}

function assembleArray(imgArray,variants,arrayOfProducts,imgsrc,nameID,metafields,id){
    sleep(1000);
    var tags = "satellite_id: " + id + ", Satellite Case";
    var productVariant = {
        product: {
            title: "Wood & Resin Phone Case - " + nameID,
            vendor: "CarvedX",
            tags: tags,
            "metafields": metafields,
            variants: variants
        }
    }

    request.post({
        url: "https://a94ee40001133cdff8516508998aa04b:525010ab04f1f91ce82be41caebee64e@carvedx.myshopify.com/admin/products.json",
        json: true,
        body: productVariant,
    }, function(error, response, body){
        if(body.errors){
            console.log("Error uploading product for " + nameID + "!");
            console.log(body);
        } else{
            console.log("No Error uploading product for " + nameID + "!")
            retreiveVariantsAndTypes(imgArray,body,id);
        }
    })


}
function retreiveVariantsAndTypes(imgArray,body){
    var productID = body.product.id
    var mainArray = [];
    var deviceArray2 = {};
    var deviceArray2Container = [];
    for(var j = 0; j < body.product.variants.length; j++){
        deviceArray2 = {};
        imgArray = {};
        var variantID = body.product.variants[j].id;
        var type = body.product.variants[j].sku;
        type = type.split("-")
        deviceArray2["variant_ids"] = [
            variantID                
        ];
        deviceArray2["type"] = type[2];
        deviceArray2Container.push(deviceArray2);
        if(j == body.product.variants.length - 1){
            retrieveMetafield(deviceArray2Container,productID);
        }
    }

}

function retrieveMetafield(deviceArray,productID){
    sleep(1000);
    request.get({
        url: "https://a94ee40001133cdff8516508998aa04b:525010ab04f1f91ce82be41caebee64e@carvedx.myshopify.com/admin/products/" + productID + "/metafields.json",
        json: true,
    }, function(error, response, body){
        if(body.errors){
            console.log("Error retrieving metafield for " + productID + "!");
            console.log(body);
        } else{
            console.log("No Error retrieving metafield for " + productID + "!")
            handleMetafields(body,deviceArray,productID);
        }
    })

}

function handleMetafields(body,deviceArray,productID){
    var finalArray = [];
    var finalObj = {};
    for(var i = 0; i < body.metafields.length; i++){
        finalObj = {};
        var deviceType = body.metafields[i].namespace;
        deviceType = deviceType.split(": ");
        deviceType = deviceType[1];
        var url = body.metafields[i].value;
        finalObj["src"] = url;
        for(var j = 0; j < deviceArray.length; j++){
            var typeToMatch = (deviceArray[j].type);
            if(deviceType == typeToMatch){
                var variantID = deviceArray[j].type
                finalObj["variant_ids"] = deviceArray[j].variant_ids;
                finalArray.push(finalObj);
            }

        }
        if(i == body.metafields.length - 1){
            uploadImage(finalArray,productID);
        }
    }


}





/*
imgArray["src"] = url;
imgArray["variant_ids"] = [
    variantID                
]
*/

function uploadImage(containerArray,productID){
    sleep(1000);
    var image = {
        "product": {
            id: productID,
            "images": containerArray
        }
    }    
    request.put({
        url: "https://a94ee40001133cdff8516508998aa04b:525010ab04f1f91ce82be41caebee64e@carvedx.myshopify.com/admin/products/" + productID + ".json",
        json: true,
        body: image,        
    }, function(error, response, body){
        if(body.errors){
            console.log("Error uploading images for " + productID + "!");
            console.log(body);
        } else{
            console.log("No Error uploading images for " + productID + "!");
            //console.log(body);
        }
    })


}









