const express = require('express')
const app = express()
const https = require('https');
const fs = require('fs');
const bodyParser = require('body-parser');
const request = require('request');
const rp = require('request-promise');
var sleep = require('system-sleep');

app.use(bodyParser.json());

app.use(bodyParser.urlencoded({
    extended: true,
}));

app.post( '/webhook', function (request, response) {
    response.send('OK');
    const theData = request.body;
    doStuffWithId(theData);
});

app.get('/webhook', function (req, res) {
    res.send("it works!");
})

https.createServer({
    key: fs.readFileSync('key.pem'),
    cert: fs.readFileSync('cert.pem'),
    //passphrase: "kpwg499j"
}, app).listen(443);
console.log("Server Started!");
            
            
function doStuffWithId(body){
    var lineItems = body.line_items;
    for(var i = 0; i < lineItems.length; i++){
        var sku = lineItems[i].sku;
        var id = lineItems[i].variant_id;
        var productID = lineItems[i].product_id;
        var correctID = 1007201058860;
        if(productID == correctID){
            console.log("Sold: " + sku);
            deleteVariant(id);
        } else {
            console.log("Sold: " + sku);
            console.log("not a sattelite product!!");
        }
    }
}
function deleteVariant(id){
    getImageId(id, function(){
        request({
            method: "DELETE",
            url: "https://a94ee40001133cdff8516508998aa04b:525010ab04f1f91ce82be41caebee64e@carvedx.myshopify.com/admin/products/1007201058860/variants/" + id + ".json",
        }, function(error, response, body){
            console.log("Deleted Variant!");
        })
    })
}
function getImageId(variantId,callback){
    request({
        url: "https://a94ee40001133cdff8516508998aa04b:525010ab04f1f91ce82be41caebee64e@carvedx.myshopify.com/admin/variants/" + variantId + ".json",
        json: true,       
    }, function(error, response, body){
        imgId = body.variant.image_id;
        deleteImage(variantId, callback,imgId);
    })
}



function deleteImage(variantId, callback,img){
    request({
        method: "DELETE",
        url: "https://a94ee40001133cdff8516508998aa04b:525010ab04f1f91ce82be41caebee64e@carvedx.myshopify.com/admin/products/1007201058860/images/" + img + ".json",
    }, function(error, response, body){
        console.log("Deleted Image!"); 
        callback(variantId);
    })

}

























